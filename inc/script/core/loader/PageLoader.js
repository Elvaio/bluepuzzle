import { sitemap } from '../../app/config/sitemap';
import ObjectAssign from 'object-assign';

class PageLoader {
	constructor(options){
		this.options = {
			viewModelsLoaded: ()=>{}
		};

		ObjectAssign(this.options, options);

		this.pages = document.querySelectorAll('.view');
		this.pagesArray = sitemap.pages;
		this.parent = null;
		this.activePages = [];
		this.parsingPage = 0;
		this.viewModels = [];

		this.getViews();
		this.preloadViewModels();
	}

	getViews(){
		if(!this.pages.length) throw('No page identifiers found');

		for(let i = 0; i < this.pages.length; i++){
			this.getPageFromSitemap(this.pages[i], this.pagesArray);
		}
	}

	getPageFromSitemap(element, pages){
		let currView = this.getViewIdentifier(element).replace('view-', '');

		for(let i = 0; i < pages.length; i++){
			let page = pages[i];

			if( page.id == currView ) {
				if(this.parent){
					page.parent = this.parent;
				}

				if( page.pages && page.pages.length ) {
					this.parent = page.id;

					this.pagesArray = page.pages;
				}

				this.activePages.push( page );

				break;
			}
		}
	}

	preloadViewModels(){
		// If all pages are parsed, apply bindings.
		if(this.parsingPage >= this.activePages.length) {
			ko.applyBindings();

			this.options.viewModelsLoaded();

			return;
		}

		let pageElement = this.pages[this.parsingPage];
		let viewID = this.getViewIdentifier( pageElement );
		let componentID = viewID.replace('view-', '');

		this.activePages[this.parsingPage].viewmodel( ( vm ) =>
		{
			pageElement.setAttribute('data-bind', 'component: ' + '\'' + componentID + '\'');

			ko.components.register(componentID, {
				viewModel: () => {
					let parent = this.viewModels[this.viewModels.length - 1] || null;
					let viewModel = new vm.default(document.querySelectorAll('.' + viewID), parent);

					this.viewModels.push(viewModel);

					return viewModel;
				},
				template: { element: pageElement },
				synchronous: true
			});

			this.parsingPage++;

			this.preloadViewModels();
		});
	}

	getViewIdentifier(element){
		let regex = /(view-[^ ]+\b)/g;

		return element.className.match(regex)[0];
	}
}

export default PageLoader;