/**
 * The sitemap lists all available pages with corresponding dependencies.
 */

export const sitemap = {
	"pages": [
		{
			"id": "index",
			"viewmodel": function(callback){ require.ensure(["../view/IndexViewModel"], (require) =>{ let vm = require("../view/IndexViewModel"); callback(vm); }, "index") },
			"pages": [
				{
					"id": "home",
					"viewmodel": function(callback){ require.ensure(["../view/home/HomeViewModel"], (require)=>{ let vm = require("../view/home/HomeViewModel"); callback(vm); }, "home") },
				}
			]
		}
	]
};
