class AbstractViewModel {
	constructor(element, parent){
		this.element = $(element);
		this.parent = parent;
		this.components = {};
	}

	registerComponent(viewModel, selector, componentNameSpace){
		let pageElements = document.querySelectorAll('.' + selector);
		let parentViewModel = this;
		let viewModelInstance = null;

		for(let i = 0; i < pageElements.length; i++){
			$(pageElements[i]).attr('data-bind', 'component: ' + '\'' + selector + ((i > 0) ? '-' + i : '') + '\'');

			ko.components.register(selector + ((i > 0) ? '-' + i : ''), {
				viewModel: () => {
					viewModelInstance = new viewModel(document.querySelectorAll('.' + selector)[i], parentViewModel);

					this.components[componentNameSpace] = viewModelInstance;

					this.onComponentReady(selector);

					return viewModelInstance;
				},
				template: { element: pageElements[i] },
				synchronous: true
			});
		}
	}

	onComponentReady(identifier){

	}
}

export default AbstractViewModel;