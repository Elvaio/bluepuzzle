import AbstractViewModel from './..//AbstractViewModel';
import FormHelper from '../../util/FormHelper';
import { TweenLite, TimelineLite, CSSPlugin, EasePack } from 'gsap';

class HomeViewModel extends AbstractViewModel {
    constructor(element, parent) {
        super( element, parent );

        this.formHelper = new FormHelper( $( '.form-contact', this.element ) );

        this._setTimelines();
    }

    _setTimelines(){

    }
}

export default HomeViewModel;
