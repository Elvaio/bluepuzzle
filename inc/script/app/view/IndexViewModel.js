import AbstractViewModel from './AbstractViewModel';
import "ScrollToPlugin";
import { TweenLite, TimelineLite, CSSPlugin, EasePack } from 'gsap';

class IndexViewModel extends AbstractViewModel {
	constructor(element, parent){
		super(element, parent);

		this.navItems = $('.header-main .nav-main a', this.element);

		this._addListeners();
	}

	_addListeners(){
		$(window).on('scroll.index', this._scrollHandler.bind(this));

		$('.header-main .nav-main', this.element).on('click', 'a', this._scrollToSection.bind(this));

		this._scrollHandler();
	}

	_scrollHandler(){
		let header = $('.header-main', this.element);

		if($(window).scrollTop() >= header.find('.contact-info').height()){
			header.addClass('fixed');
		}else{
			header.removeClass('fixed');
		}

		this._updateNav();
	}

	_updateNav(){
		for(let i = 0; i < this.navItems.length; i++){
			let id = $(this.navItems[i]).attr('href');

			if($(window).scrollTop() >= ($(id).offset().top - $('.header-main', this.element).height())){
				this.navItems.removeClass('active');
				$("[href='" + id + "']", $('.header-main', this.element)).addClass('active');
			}
		}

		if(($(window).scrollTop() + $(window).height()) >= this.element.height()){
			this.navItems.removeClass('active');
			$(this.navItems[this.navItems.length - 1]).addClass('active');
		}
	}

	_scrollToSection(event){
		if(event){
			event.preventDefault();

			let element = $(event.currentTarget);
			let id = element.attr('href');

			TweenLite.to($(window), 0.5, { scrollTo: { y: ($(id).offset().top - $('.header-main', this.element).height()) }, ease:Quint.easeOut });
		}
	}
}

export default IndexViewModel;
