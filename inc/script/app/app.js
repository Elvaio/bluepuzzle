// This needs to be imported here so webpack knows what to do with the scss.
require('../../style/screen.scss');

// Workaround for libs that have AMD logic. (Will be removed with ES6 convertion)
import $ from 'jquery';
import jQuery from 'jquery';

window.jQuery = jQuery;
window.$ = $;

/* ONLY NEW CODE HERE */
import PageLoader from '../core/loader/PageLoader';
import ko from 'knockout';
import device from 'device.js/device';

window['ko'] = ko;

ko.bindingHandlers['tap'] = {
	'init': function (element, valueAccessor, allBindingsAccessor, viewModel) {
		$(element).on('click', function (e) {
			e.preventDefault();
			valueAccessor().call(viewModel, viewModel, e);
		});
	}
};

let pageLoader = new PageLoader({
	viewModelsLoaded: ()=>{

	}
});
