require('jquery-validation/dist/jquery.validate');

class FormHelper{
	constructor(form) {
		this.form = form;
		this.formRules = {};
		this.loading = false;

		if(this.form.hasClass('ignore-validation')) return;

		this.addListeners();
		this.setCustomRules();
		this.getRules();
	}

	addListeners(){
		this.form.on('blur', 'input, textarea', this.checkFocus.bind(this));
	}

	checkFocus(event){
		let input = $(event.currentTarget);

		console.log(input.val());

		if(input.val() != '') {
			input.addClass( 'filled' );
		} else {
			input.removeClass( 'filled' );
		}
	}

	setCustomRules(){
		$.validator.addMethod("customemail", function(value, element) {
			return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i.test(value);
		}, "Sorry, We've enabled very strict email validation");
	}

	getRules(){
		let $fields = $('[data-validation]', this.form);

		for(let i = 0; i < $fields.length; i++){
			let $field = $($fields[i]);
			let fieldName = $field.attr('name');
			let rules = $field.attr('data-validation').split('|');
			let obj = {};

			for(let j = 0; j < rules.length; j++){
				let rule = rules[j].split(':');
				obj[rule[0]] = rule.length > 1 ? rule[1] : true;
			}

			this.formRules[fieldName] = obj;
		}

		this.setValidation();
	}

	setValidation(){
		this.form.validate({
			rules: this.formRules,
			errorClass:'validation-failed',
			errorPlacement: function(error, element) {
				$(element).parents('.input, .textarea').addClass('validation-failed');
			},
			unhighlight: function(element, errorClass, validClass) {
				var element = $(element);
				$(element).parents('.input, .textarea').removeClass('validation-failed');
			},
			submitHandler: this.promptEmailClient.bind(this),
			invalidHandler: ()=>{
				//console.log(arguments);
			}
		});
	}

	promptEmailClient(){
		let data = this.form.serializeArray();
		let dataObj = {};

		for(let i = 0; i < data.length; i++){
			dataObj[data[i].name] = data[i].value;
		}

		window.location.href = 'mailto:info@blue-puzzle.nl?Subject=' + dataObj.subject + '&Body=' + dataObj.message + '%0A%0A' + 'Met vriendelijke groet,%0A ' + dataObj.firstname + ' ' + dataObj.lastname;
	}
}

export default FormHelper;