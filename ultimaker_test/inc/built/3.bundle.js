webpackJsonp([3],{

/***/ 8:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _Gateway = __webpack_require__(9);
	
	var _Gateway2 = _interopRequireDefault(_Gateway);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var AbstractViewModel = function AbstractViewModel(element) {
		_classCallCheck(this, AbstractViewModel);
	
		this.element = element;
		this.gateway = _Gateway2.default.getInstance();
		this.subscriptions = [];
	};
	
	exports.default = AbstractViewModel;

/***/ },

/***/ 9:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Gateway = function () {
		function Gateway() {
			_classCallCheck(this, Gateway);
		}
	
		_createClass(Gateway, [{
			key: 'postData',
			value: function postData(url, data, callback) {
				this._requestData(url, data, callback, 'POST');
			}
		}, {
			key: 'getData',
			value: function getData(url, data, callback) {
				this._requestData(url, data, callback, 'GET');
			}
		}, {
			key: '_requestData',
			value: function _requestData(url, data, callback, method) {
				$.ajax({
					url: url,
					method: method,
					data: data,
					complete: function complete(resp, status) {
						callback(resp, status);
					}
				});
			}
		}], [{
			key: 'getInstance',
			value: function getInstance() {
				if (!Gateway._instance) {
					Gateway._instance = new Gateway();
				}
	
				return Gateway._instance;
			}
		}]);
	
		return Gateway;
	}();
	
	exports.default = Gateway;

/***/ },

/***/ 10:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _AbstractViewModel2 = __webpack_require__(8);
	
	var _AbstractViewModel3 = _interopRequireDefault(_AbstractViewModel2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var LandingViewModel = function (_AbstractViewModel) {
		_inherits(LandingViewModel, _AbstractViewModel);
	
		function LandingViewModel(element) {
			_classCallCheck(this, LandingViewModel);
	
			var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(LandingViewModel).call(this, element));
	
			_this.introTimeline = null;
			_this.formTimeline = null;
	
			_this.introVisible = ko.observable(true);
			_this.stepIndicator = ko.observable(1);
			_this.uploads = ko.observableArray([]);
			_this.activeUpload = ko.observable(0);
			_this.userData = ko.observable({
				firstname: '',
				lastname: '',
				email: '',
				address: '',
				zipcode: '',
				city: '',
				optin: ''
			});
	
			_this.setTimelines();
			_this.addNewUpload();
			return _this;
		}
	
		_createClass(LandingViewModel, [{
			key: 'setTimelines',
			value: function setTimelines() {
				if (this.introVisible() && !this.mainTimeline) {
					this.introTimeline = new TimelineLite({ paused: true });
	
					this.introTimeline.set($('.cta-main', this.element), { display: 'block' });
					this.introTimeline.staggerFrom($('.cta-main .heading-01 .word', this.element), 1, {
						paddingTop: $('.cta-main .heading-01 .word', this.element).height(),
						ease: Quint.easeOut
					}, 0.2);
					this.introTimeline.staggerFrom($('.cta-main .copy-01, .cta-main .btn', this.element), 0.3, {
						y: 30,
						opacity: 0
					}, 0.2, '-=0.4');
	
					this.introTimeline.play();
				}
	
				if (!this.introVisible() && !this.formTimeline) {
					this.formTimeline = new TimelineLite({ paused: true });
	
					this.formTimeline.from($('.form-holder', this.element), 0.5, { y: -50, opacity: 0, ease: Quint.easeOut });
	
					this.formTimeline.play();
				}
			}
		}, {
			key: 'loadForm',
			value: function loadForm() {
				var _this2 = this;
	
				this.introTimeline.duration(0.5);
				this.introTimeline.reverse();
				this.introTimeline.eventCallback('onReverseComplete', function () {
					_this2.introVisible(false);
	
					_this2.setTimelines();
				});
			}
		}, {
			key: 'updateStatus',
			value: function updateStatus(item, event) {
				var filename = $(event.currentTarget).val();
				var lastIndex = filename.lastIndexOf("\\");
				if (lastIndex >= 0) {
					filename = filename.substring(lastIndex + 1);
				}
	
				item.name(filename);
			}
		}, {
			key: 'addNewUpload',
			value: function addNewUpload() {
				this.uploads.push({
					name: ko.observable('No File Selected'),
					w: ko.observable(),
					h: ko.observable(),
					d: ko.observable(),
					color: ko.observable('black'),
					quality: ko.observable('low')
				});
			}
		}, {
			key: 'removeUpload',
			value: function removeUpload(item) {
				this.uploads.remove(item);
			}
		}, {
			key: 'nextStep',
			value: function nextStep() {
				var _this3 = this;
	
				this.validateFieldset(function () {
					_this3.stepIndicator(_this3.stepIndicator() + 1);
				});
			}
		}, {
			key: 'prevStep',
			value: function prevStep() {
				this.stepIndicator(this.stepIndicator() - 1);
			}
		}, {
			key: 'validateFieldset',
			value: function validateFieldset(callback) {
				var isValid = false;
	
				switch (this.stepIndicator()) {
					case 1:
						{
							for (var i = 0; i < this.uploads().length; i++) {
								if (this.uploads()[i].name() == 'No File Selected') {
									alert('Please select a file!');
									isValid = false;
									break;
								} else {
									isValid = true;
								}
							}
						}
						break;
					case 3:
						{
							this.validateUserDetails(callback);
						}
						break;
					default:
						{
							callback();
						}
				}
	
				if (isValid) {
					callback();
				}
			}
		}, {
			key: 'validateUserDetails',
			value: function validateUserDetails(callback) {
				var _this4 = this;
	
				$('.form-upload').validate({
					rules: {
						firstname: {
							required: true
						},
						lastname: {
							required: true
						},
						email: {
							required: true,
							email: true
						},
						address: {
							required: true
						},
						zipcode: {
							required: true
						},
						city: {
							required: true
						},
						optin: {
							required: true
						}
					},
					errorClass: 'validation-failed',
					errorPlacement: function errorPlacement(error, element) {
						if (element.is(':checkbox') || element.is(':radio')) {
							$(element).parents('.checker').addClass('validation-failed');
						} else {
							return true;
						}
					},
					unhighlight: function unhighlight(element, errorClass, validClass) {
						var element = $(element);
						if (element.is(':checkbox') || element.is(':radio')) {
							$(element).parents('.checker').removeClass('validation-failed');
						}
						$(element).removeClass('validation-failed');
					},
					submitHandler: function submitHandler() {
						var arr = [];
	
						for (var i = 0; i < _this4.uploads().length; i++) {
							var upload = _this4.uploads()[i];
	
							arr.push({
								name: upload.name(),
								w: upload.w(),
								h: upload.h(),
								d: upload.d(),
								color: upload.color(),
								quality: upload.quality()
							});
						}
	
						console.log({
							uploads: arr,
							userData: _this4.userData()
						});
						alert('Check the console to see what data could be sent');
					}
				});
	
				$('.form-upload').submit();
			}
		}]);
	
		return LandingViewModel;
	}(_AbstractViewModel3.default);
	
	exports.default = LandingViewModel;

/***/ }

});
//# sourceMappingURL=3.bundle.js.map