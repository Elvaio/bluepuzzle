webpackJsonp([1],{

/***/ 7:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _AbstractViewModel2 = __webpack_require__(8);
	
	var _AbstractViewModel3 = _interopRequireDefault(_AbstractViewModel2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var IndexViewModel = function (_AbstractViewModel) {
		_inherits(IndexViewModel, _AbstractViewModel);
	
		function IndexViewModel(element) {
			_classCallCheck(this, IndexViewModel);
	
			var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(IndexViewModel).call(this, element));
	
			_this.navTimeline = null;
			_this.navIsOpen = false;
	
			_this.setTimelines();
			_this.addListeners();
			_this.checkInitialFilled();
			return _this;
		}
	
		_createClass(IndexViewModel, [{
			key: 'setTimelines',
			value: function setTimelines() {
				var _this2 = this;
	
				this.navTimeline = new TimelineLite({
					paused: true,
					onStart: function onStart() {
						_this2.navIsOpen = true;
						$('.nav-toggler').addClass('active');
					},
					onReverseComplete: function onReverseComplete() {
						_this2.navIsOpen = false;
						$('.nav-toggler').removeClass('active');
					}
				});
	
				this.navTimeline.set($('.header-main', this.element), { display: 'block' });
				this.navTimeline.from($('.header-main', this.element), 0.4, { scale: 0.5, opacity: 0, ease: Quint.easeOut });
				this.navTimeline.staggerFrom($('.header-main li', this.element), 0.4, { paddingTop: $('.header-main li').height() }, 0.1);
			}
		}, {
			key: 'addListeners',
			value: function addListeners() {
				var _this3 = this;
	
				var $body = $('body');
	
				$body.on('change', 'input, textarea', function (event) {
					var $input = $(event.currentTarget);
	
					_this3.setFilled($input);
				});
			}
		}, {
			key: 'checkInitialFilled',
			value: function checkInitialFilled() {
				var $inputs = $('form input, form textarea');
	
				for (var i = 0; i < $inputs.length; i++) {
					var $input = $($inputs[i]);
					this.setFilled($input);
				}
			}
		}, {
			key: 'setFilled',
			value: function setFilled($input) {
				if ($input.val().replace(/ /g, '')) {
					$input.addClass('filled');
				} else {
					$input.removeClass('filled');
				}
			}
		}, {
			key: 'toggleNav',
			value: function toggleNav() {
				this.navIsOpen ? this.navTimeline.reverse() : this.navTimeline.play();
			}
		}]);
	
		return IndexViewModel;
	}(_AbstractViewModel3.default);
	
	exports.default = IndexViewModel;

/***/ },

/***/ 8:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _Gateway = __webpack_require__(9);
	
	var _Gateway2 = _interopRequireDefault(_Gateway);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var AbstractViewModel = function AbstractViewModel(element) {
		_classCallCheck(this, AbstractViewModel);
	
		this.element = element;
		this.gateway = _Gateway2.default.getInstance();
		this.subscriptions = [];
	};
	
	exports.default = AbstractViewModel;

/***/ },

/***/ 9:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Gateway = function () {
		function Gateway() {
			_classCallCheck(this, Gateway);
		}
	
		_createClass(Gateway, [{
			key: 'postData',
			value: function postData(url, data, callback) {
				this._requestData(url, data, callback, 'POST');
			}
		}, {
			key: 'getData',
			value: function getData(url, data, callback) {
				this._requestData(url, data, callback, 'GET');
			}
		}, {
			key: '_requestData',
			value: function _requestData(url, data, callback, method) {
				$.ajax({
					url: url,
					method: method,
					data: data,
					complete: function complete(resp, status) {
						callback(resp, status);
					}
				});
			}
		}], [{
			key: 'getInstance',
			value: function getInstance() {
				if (!Gateway._instance) {
					Gateway._instance = new Gateway();
				}
	
				return Gateway._instance;
			}
		}]);
	
		return Gateway;
	}();
	
	exports.default = Gateway;

/***/ }

});
//# sourceMappingURL=1.bundle.js.map