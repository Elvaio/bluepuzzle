class Gateway {
	constructor(){

	}

	static getInstance(){
		if(!Gateway._instance){
			Gateway._instance = new Gateway();
		}

		return Gateway._instance;
	}

	postData(url, data, callback){
		this._requestData(url, data, callback, 'POST');
	}

	getData(url, data, callback){
		this._requestData(url, data, callback, 'GET');
	}

	_requestData(url, data, callback, method){
		$.ajax({
			url: url,
			method: method,
			data: data,
			complete:(resp, status)=>{
				callback(resp, status)
			}
		});
	}
}

export default Gateway;