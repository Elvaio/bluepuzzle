require('../../style/screen.scss');

import ko from 'knockout';
import $ from 'jquery';
import device from 'device.js/device';
import PageLoader from '../lib/utils/PageLoader';

window['$'] = $;
window['jQuery'] = $;
window['ko'] = ko;
window['device'] = device;

// Class inheritance patch for Babel
(function() {
	if (!(Object.setPrototypeOf || {}.__proto__)) {
		var nativeGetPrototypeOf = Object.getPrototypeOf;

		Object.getPrototypeOf = function(object) {
			if (object.__proto__) {
				return object.__proto__;
			} else {
				return nativeGetPrototypeOf.call(Object, object);
			}
		}
	}
})();
// End Class inheritance fix for Babel

// Let Knockout know that we will take care of managing the bindings of our children
ko.bindingHandlers.stopBinding = {
	init: function() {
		return { controlsDescendantBindings: true };
	}
};

// KO 2.1, now allows you to add containerless support for custom bindings
ko.virtualElements.allowedBindings.stopBinding = true;

// Create custom binding to proxy click events to touch events for better mobile interaction.
ko.bindingHandlers['tap'] = {
	'init': function (element, valueAccessor, allBindingsAccessor, viewModel) {
		$(element).on(device().mobile() || device().tablet() ? 'touchstart' : 'click', function (e) {
			e.preventDefault();
			valueAccessor().call(viewModel, viewModel, e);
		});
	}
};

require([
	'jquery-validation/dist/jquery.validate',
	'jquery-validation/dist/additional-methods',
	'gsap/src/minified/TweenLite.min',
	'gsap/src/minified/TimelineLite.min',
	'gsap/src/minified/plugins/CSSPlugin.min',
], ()=>{
	// App startup
	const pageLoader = new PageLoader();
});