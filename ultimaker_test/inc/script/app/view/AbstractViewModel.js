import Gateway from '../net/Gateway';

class AbstractViewModel {
	constructor(element){
		this.element = element;
		this.gateway = Gateway.getInstance();
		this.subscriptions = [];
	}
}

export default AbstractViewModel;