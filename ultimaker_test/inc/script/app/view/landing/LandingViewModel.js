import AbstractViewModel from '../AbstractViewModel';

class LandingViewModel extends AbstractViewModel {
	constructor(element){
		super(element);

		this.introTimeline = null;
		this.formTimeline = null;

		this.introVisible = ko.observable(true);
		this.stepIndicator = ko.observable(1);
		this.uploads = ko.observableArray([]);
		this.activeUpload = ko.observable(0);
		this.userData = ko.observable({
			firstname: '',
			lastname: '',
			email: '',
			address: '',
			zipcode: '',
			city: '',
			optin: ''
		});

		this.setTimelines();
		this.addNewUpload();
	}

	setTimelines(){
		if(this.introVisible() && !this.mainTimeline)
		{
			this.introTimeline = new TimelineLite( {paused: true} );

			this.introTimeline.set( $( '.cta-main', this.element ), {display: 'block'} );
			this.introTimeline.staggerFrom( $( '.cta-main .heading-01 .word', this.element ), 1, {
				paddingTop: $( '.cta-main .heading-01 .word', this.element ).height(),
				ease: Quint.easeOut
			}, 0.2 );
			this.introTimeline.staggerFrom( $( '.cta-main .copy-01, .cta-main .btn', this.element ), 0.3, {
				y: 30,
				opacity: 0
			}, 0.2, '-=0.4' );

			this.introTimeline.play();
		}

		if(!this.introVisible() && !this.formTimeline){
			this.formTimeline = new TimelineLite({ paused: true });

			this.formTimeline.from($('.form-holder', this.element), 0.5, { y: -50, opacity: 0, ease:Quint.easeOut });

			this.formTimeline.play();
		}
	}

	loadForm(){
		this.introTimeline.duration(0.5);
		this.introTimeline.reverse();
		this.introTimeline.eventCallback('onReverseComplete', ()=>{
			this.introVisible(false);

			this.setTimelines();
		});
	}

	updateStatus(item, event){
		let filename = $(event.currentTarget).val();
		let lastIndex = filename.lastIndexOf("\\");
		if (lastIndex >= 0) {
			filename = filename.substring(lastIndex + 1);
		}

		item.name(filename);
	}

	addNewUpload(){
		this.uploads.push({
			name: ko.observable('No File Selected'),
			w: ko.observable(),
			h: ko.observable(),
			d: ko.observable(),
			color: ko.observable('black'),
			quality: ko.observable('low')
		});
	}

	removeUpload(item){
		this.uploads.remove(item);
	}

	nextStep(){
		this.validateFieldset(()=>{
			this.stepIndicator(this.stepIndicator() + 1);
		});
	}

	prevStep(){
		this.stepIndicator(this.stepIndicator() - 1);
	}

	validateFieldset(callback){
		let isValid = false;

		switch(this.stepIndicator()){
			case 1: {
				for(let i = 0; i < this.uploads().length; i++){
					if(this.uploads()[i].name() == 'No File Selected'){
						alert('Please select a file!');
						isValid = false;
						break;
					}else{
						isValid = true;
					}
				}
			}
			break;
			case 3: {
				this.validateUserDetails(callback);
			}
			break;
			default: {
				callback()
			}
		}

		if(isValid){
			callback();
		}
	}

	validateUserDetails(callback){
		$('.form-upload').validate({
			rules: {
				firstname: {
					required: true
				},
				lastname: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				address: {
					required: true
				},
				zipcode: {
					required: true
				},
				city: {
					required: true
				},
				optin: {
					required: true
				}
			},
			errorClass:'validation-failed',
			errorPlacement: function(error, element) {
				if (element.is(':checkbox') || element.is(':radio')) {
					$(element).parents('.checker').addClass('validation-failed');
				}
				else {
					return true;
				}
			},
			unhighlight: function(element, errorClass, validClass) {
				var element = $(element);
				if (element.is(':checkbox') || element.is(':radio')) {
					$(element).parents('.checker').removeClass('validation-failed');
				}
				$(element).removeClass('validation-failed');
			},
			submitHandler: ()=>{
				let arr = [];

				for(let i = 0; i < this.uploads().length; i++){
					let upload = this.uploads()[i];

					arr.push({
						name: upload.name(),
						w: upload.w(),
						h: upload.h(),
						d: upload.d(),
						color: upload.color(),
						quality: upload.quality()
					});
				}

				console.log({
					uploads: arr,
					userData: this.userData()
				});
				alert('Check the console to see what data could be sent');
			}
		});

		$('.form-upload').submit();
	}
}

export default LandingViewModel;
