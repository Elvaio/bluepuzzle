import AbstractViewModel from '../AbstractViewModel';

class IndexViewModel extends AbstractViewModel {
	constructor(element){
		super(element);

		this.navTimeline = null;
		this.navIsOpen = false;

		this.setTimelines();
		this.addListeners();
		this.checkInitialFilled();
	}

	setTimelines(){
		this.navTimeline = new TimelineLite({
			paused: true,
			onStart: ()=>{
				this.navIsOpen = true;
				$('.nav-toggler').addClass('active');
			},
			onReverseComplete: ()=>{
				this.navIsOpen = false;
				$('.nav-toggler').removeClass('active');
			}
		});

		this.navTimeline.set($('.header-main', this.element), { display: 'block' });
		this.navTimeline.from($('.header-main', this.element), 0.4, { scale: 0.5, opacity: 0, ease:Quint.easeOut });
		this.navTimeline.staggerFrom($('.header-main li', this.element), 0.4, { paddingTop: $('.header-main li').height() }, 0.1);
	}

	addListeners(){
		var $body = $('body');

		$body.on('change', 'input, textarea', (event)=>{
			let $input = $(event.currentTarget);

			this.setFilled($input);
		});
	}

	checkInitialFilled(){
		let $inputs = $('form input, form textarea');

		for(let i = 0; i < $inputs.length; i++){
			let $input = $($inputs[i]);
			this.setFilled($input);
		}
	}

	setFilled($input){
		if($input.val().replace(/ /g,'')){
			$input.addClass('filled');
		}else{
			$input.removeClass('filled');
		}
	}

	toggleNav(){
		this.navIsOpen ? this.navTimeline.reverse() : this.navTimeline.play();
	}
}

export default IndexViewModel;