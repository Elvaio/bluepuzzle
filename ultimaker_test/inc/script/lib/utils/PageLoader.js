import $ from 'jquery';
import ko from 'knockout';

class PageLoader {
	constructor(){
		this.$pages = $('.view');
		this.activePages = [];
		this.viewModels = {};

		this.readPages();
	}

	readPages(){
		for(let i = 0; i < this.$pages.length; i++){
			let pageElement = $(this.$pages[i]);
			this.activePages.push(pageElement);
			this.requireDeps(pageElement, this.$pages[i]);
		}
	}

	requireDeps(pageElement, element){
		let viewModel = null;

		// NOTE: This has to be done like this unfortunately because of webpack;
		switch(true) {
			case pageElement.hasClass( 'view-index' ):
				{
					require.ensure( [], ()=>
					{
						viewModel = require( '../../app/view/index/IndexViewModel' );
					this.initializeViewModel(viewModel, element, 'index');
				});
			}
			break;
			case pageElement.hasClass( 'view-landing' ):
				{
					require.ensure( [], ()=>
					{
						viewModel = require( '../../app/view/landing/LandingViewModel' );
					this.initializeViewModel(viewModel, element, 'landing');
				});
			}
			break;
		}
	}

	initializeViewModel(viewModel, element, pageName){
		let instance = new viewModel.default(element);
		ko.applyBindings(instance, element);
		this.viewModels[pageName] = instance;
	}
}

export default PageLoader;