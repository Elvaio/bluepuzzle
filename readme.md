# Ultimaker - Upload your design

### Introduction
This application is built for an assessment for Ultimaker on behalf of BluePuzzle.
The app allows a user to upload multiple files using a form. The form has been devided into three fieldsets. The first one allows the user to add, remove, or update a file.
In the second fieldset, the user is able to provide some configuration settings for each individual upload, using the dropdown in the top of this fieldset.
When the user is done with providing their configurations, they'll be sent to the last fieldset. At this point the user provides personal details and after the form is being submitted, all it's data will be sent to the backend.
For demo purposes, you'll be seeing an alert message with instructions of how to see the data that I've collected from the form (and could be send to some kind of REST API using a POST request).

For this assessment, I've used ES6 instead of ES5. With the main reason because I've been working with TypeScript for 3 years and ES6 works very similar. Aside from all the other reasons like, ES6 being the future of JavaScript, proper class inheritance support, arrow functions, destructuring, block scoped variables and much more... I just wanted to show (off) that I'm able to work with the latest technologies.

### Used libraries

  - jQuery
  - jQuery Validation
  - DeviceJS
  - Knockout
  - TweenLite/TimelineLite

### Motivation regarding choice of libraries:

Using twig, it could be convenient to have a robust and powerfull view engine for frontend operations.
Knockout is a really well documented and usable framework for view rendering. The main reason why I choose this library is because of its 'unique' syntax which will never interfere with the twig syntax or any other server side template rendering engine.

I mainly use jQuery for measurement and queryselectors. Also the $.ajax function is really well built by jQuery.

The GSAP library enables you to show some extra love to you're frontend by enabling you to make some awesome and high performing animations.
TweenLite and TimelineLite scales really well for all different kind of usecases.

### Without keeping twig in mind

If I would have to build a tool without having to work with twig templates or other serverside rendered templating engines, I'd choose a custom build stack existing from:

- React (view engine)
- Redux (flux architecture)
- GSAP (TweenLite/TimelineLite)

The great advantages of React is mainly thinking in components. And offcourse the huge performance gain using virtual DOM.
I really like to devide and isolate everything into its own component.
The main reason for this, is to be able to easily get components in or out of a page (or parent component) without having to change any logic around the component.

Redux is a simple amendment of the flux architecture.
MVC has been around for quite some time... In frontend however, it often can be confusing when to use a Model or Controller and determining what purpose they should have.
To accomplish all of that, you're working with a lot of classes which are inheriting other classes.
Eventually you'll and up with a huge amount of classes and are getting to unnescasery data.
(for example we're trying to get a (property) banana... instead of just getting a banana, we'll end up with a banana, the hand of a monkey, the monkey itself and the whole rainforest with all it's animals).

Flux is mainly focussed on a functional design pattern. It allows you to do just one specific thing without getting to many unwanted factors with that single operation.
By doing all of the operations in a single direction (one way dataflow), you enable your application to scale a lot better compared to an MVC pattern.
