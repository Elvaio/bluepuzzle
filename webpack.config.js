var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var path = require('path');
var root = __dirname;
var gsapPath = "/node_modules/gsap/src/uncompressed/";
var sassLoaders = [
	'css-loader?sourceMap&url=false',
	'postcss-loader',
	'sass-loader'
];

module.exports = {
	context: __dirname,
	cache: true,
	devtool: 'source-map',
	entry: './inc/script/app/app.js',
	output: {
		filename: '[name].bundle.js',
		chunkFilename: "[name].bundle.js",
		path: './inc/built',
		publicPath: './inc/built/'
	},
	module: {
		loaders: [
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract('style-loader', sassLoaders.join('!'))
			},
			{
				test: /\.js?$/,
				exclude: /(node_modules|bower_components)/,
				loaders: [
					'babel?presets[]=es2015'
				]
			}
		]
	},
	plugins: [
		new ExtractTextPlugin('[name].screen.css')
	],
	postcss: [
		autoprefixer({
			browsers: ['last 2 versions', 'ie >= 9']
		})
	],
	node: {
		fs: "empty"
	},
	resolve: {
		root: path.resolve(root),
		extensions: ['', '.json', '.jsx', '.js'],
		alias: {
			"TweenLite": "gsap",
			"CSSPlugin": "gsap",
			"Draggable": path.join(root, gsapPath + "utils/Draggable.js"),
			"ScrollToPlugin": path.join(root, gsapPath + "plugins/ScrollToPlugin.js"),
			"EasePack": path.join(root, gsapPath + "easing/EasePack.js")
		},
		modulesDirectories: [
			'node_modules',
			'bower_components',
			'./web/vendor',
			'./web/vendor/blueimp-file-upload/js/vendor'
		]
	}
};